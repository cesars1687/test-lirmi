
## Solución

La solución para esta prueba fue leer las dos página del API y unirlas en uno solo, luego mostrarla en un listado y el filtro
por Id se aplica a la coleccion creada en base a las dos llamadas.

Podes visualizarlo en <http://test-lirmi.cesarherbozo.com/users-api>

## Instalación

Se uso Laravel 8 y base de datos Sqlite.

Seguir los siguientes pasos:

- clonar el repositorio git clone https://bitbucket.org/cesars1687/test-lirmi.git .
- composer install .
- crear archivo .env .
- php artisan key:generate .
- php artisan cache:clear .
- habilitar extension=pdo_sqlite .
- sudo chmod 777 /database/database.sqlite .
- php artisan migrate .

Luego de ello ir a la ruta http://{{ host }}/users-api y a probar!!

