@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Listado de Usuarios</div>
                    <div class="card-body">
                        <users-api-component></users-api-component>
                        <a href="{{ route('users.index') }}" target="_blank">Ver Usuarios Registrados</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection