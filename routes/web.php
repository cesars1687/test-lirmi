<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\UserAPIController;
use \App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('users', [\App\Http\Controllers\UserController::class, 'index']);
Route::get('users', [\App\Http\Controllers\UserController::class, 'index']);*/
Route::get('get-users-api', [UserAPIController::class, 'getUsersAPI']);
Route::resource('users-api', UserAPIController::class);
Route::get('get-users', [UserController::class , 'getUsers'])->name('users.get-users');
Route::resource('users', UserController::class);
