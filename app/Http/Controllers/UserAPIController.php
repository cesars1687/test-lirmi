<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class UserAPIController extends Controller
{
    public function index()
    {
        return view('user-api.index');
    }

    public function getUsersAPI(Request $request)
    {
        $users_1 = Http::get('https://reqres.in/api/users',  [
            'page' => 1,
        ]);
        $users_2 =  Http::get('https://reqres.in/api/users',  [
            'page' => 2,
        ]);
        $users_1 = $users_1->object()->data;
        $users_2 = $users_2->object()->data;

        $data = collect($users_1)->merge(collect($users_2));

        if(!empty($request->search)){
            return response()->json($data->where('id', '=', $request->search)->flatten());
        }
        return $data->toJson();
    }

}
