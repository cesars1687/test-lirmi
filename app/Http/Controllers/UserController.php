<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function index()
    {
        return view('user.index');
    }

    public function getUsers()
    {
        return User::all();
    }

    public function store(Request $request)
    {
        try{
            return response()->json([
                'status' => true,
                'data' => User::create($request->user)
            ]);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            return response()->json(['status' => false, 'message' => 'Ocurrío un Error']);
        }

    }

    public function destroy($id)
    {
        try {
            return response()->json([
                'status' => true,
                'data' => User::find($id)->delete()
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json(['status' => false, 'message' => 'Ocurrío un Error']);
        }
    }

}
